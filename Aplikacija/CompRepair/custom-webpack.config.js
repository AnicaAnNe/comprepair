const NodePolyfillPlugin = require("node-polyfill-webpack-plugin")

module.exports = {
    resolve: {
        fallback: {
          fs: false,
          tls: false,
          net: false,
          path: false,
          zlib: false,
          http: false,
          https: false,
          stream: false,
          crypto: false,
          "crypto-browserify": false,
        //   worker_threads: false,
        //   "fast-crc32c": false,
          child_process: false,
          "os": require.resolve("os-browserify/browser") ,
        //   constants: false
        } 
      },
	// Other rules...
	plugins: [
		new NodePolyfillPlugin()
	]
}