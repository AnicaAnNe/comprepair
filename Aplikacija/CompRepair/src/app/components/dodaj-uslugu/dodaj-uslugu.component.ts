import { Component, OnInit } from '@angular/core';
import { take, tap } from 'rxjs/operators';
import { DatabaseService } from 'src/app/services/database.service';
import { Usluge } from 'src/model/usluge';

@Component({
  selector: 'app-dodaj-uslugu',
  templateUrl: './dodaj-uslugu.component.html',
  styleUrls: ['./dodaj-uslugu.component.scss'],
})
export class DodajUsluguComponent implements OnInit {

  add = false;
  imeUsluge: string
  constructor(private database: DatabaseService) { }

  ngOnInit() {}

  dodaj(){
    this.add = true;
  }
  odustani(){
    this.add = false;
  }

  dodajUslugu(){
    this.database.getAllUsluge()
    .pipe(
      take(1),
    )
    .subscribe( usluge => {
      if(!usluge.find( (usluga: any) => usluga.name === this.imeUsluge)){
        this.database.addUsluga({name: this.imeUsluge, pointers: []});
        this.add = false;
        this.imeUsluge = '';
    }});
  }
}
