import { animate, state, style, transition, trigger } from '@angular/animations';

export const animation = 
trigger('fadeInOut', [
  state('in', style({
    height: '*',
  })),
  state('out', style({
    height: '0px',
  })),
  transition('in => out', animate('400ms ease-in-out')),
  transition('out => in', animate('400ms ease-in-out'))
])
// trigger('fadeInOut', [
//     transition(':enter', [
//       style({height:0}),
//       animate(500, style({height: '*'})) 
//     ]),
//     transition(':leave', [
//       animate(500, style({height:0})) 
//     ])
//   ]);