import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { forkJoin, Observable } from 'rxjs';
import { DatabaseService } from 'src/app/services/database.service';
import { StoreService } from 'src/app/services/store.service';
import {animation} from '../drop-down-item/drop-down-item-anim'
@Component({
  animations: [animation],
  selector: 'drop-down-item',
  templateUrl: './drop-down-item.component.html',
  styleUrls: ['./drop-down-item.component.scss'],
})
export class DropDownItemComponent implements OnInit {

  @Input() imeUsluge;
  @Input() pointeriNaServise;
  servisiKojiNudeUslugu: Observable<any>;
  selected: boolean;
  dropDownOpen: string;
  profileUrl: Observable<any[]>;

  constructor(private database: DatabaseService,
    private router: Router,
    private route: ActivatedRoute,
    private store: StoreService) {

    this.selected = false;
    this.dropDownOpen = 'out';
  }

  ngOnInit() {}

  click(){
    this.store.searching = true;
    this.selected = !this.selected;

    this.dropDownOpen = this.dropDownOpen === 'out' ? 'in' : 'out';
    if(!this.servisiKojiNudeUslugu && this.pointeriNaServise){
      this.servisiKojiNudeUslugu = this.database.getServicesByReference(this.pointeriNaServise);
    }
    this.profileUrl = forkJoin(this.pointeriNaServise.map((pointer) => this.database.getFileUrl(`users/${pointer}/profile.jpg`)));
  }

  openProfile(index){
    this.store.searchUser = this.database.getUser(this.pointeriNaServise[index]);
    this.router.navigate(['profile'], { relativeTo: this.route});
  }



}
