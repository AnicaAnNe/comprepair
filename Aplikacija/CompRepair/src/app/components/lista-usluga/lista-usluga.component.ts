import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { DatabaseService } from 'src/app/services/database.service';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-lista-usluga',
  templateUrl: './lista-usluga.component.html',
  styleUrls: ['./lista-usluga.component.scss'],
})
export class ListaUslugaComponent implements OnInit {

  @Input() usluge;
  dodajUsluge$: Observable<any>;
  add = false;
  imeUsluge = '';
  constructor(private database: DatabaseService,
              public store: StoreService) { }

  ngOnInit() {}

  dodaj(){
    this.add = true;
  }
  odustani(){
    this.add = false;
  }
  pretraziUsluge(){
    this.dodajUsluge$ = this.database.getUslugaByShopName(this.imeUsluge);
  }
  dodajUslugu(usluga){

        this.store.user.subscribe(usr => {
            if(usr.pruzaneUsluge.find( el => el === usluga.name) === undefined && this.add){
              this.database.updateUsluga({...usluga, pointers : [...usluga.pointers, usr.id ]});
              this.database.updateUser({...usr, pruzaneUsluge: [...usr.pruzaneUsluge, usluga.name]});
              this.add = false;
            }});
  }
}
