import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DatabaseService } from 'src/app/services/database.service';

@Component({
  selector: 'notifikacija',
  templateUrl: './notifikacija.component.html',
  styleUrls: ['./notifikacija.component.scss'],
})
export class NotifikacijaComponent implements OnInit {

  @Input() notifikacija;
  public sender: Observable<any>;
  constructor(private database: DatabaseService) { }



  ngOnInit() {
    this.sender = this.database.getUser(this.notifikacija.sender);
  }


}
