import { Component, Input, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { DatabaseService } from 'src/app/services/database.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss'],
})
export class RequestComponent implements OnInit {

  @Input() userId: string;
  @Input() sendTo: string;
  @Input() message: string;
  @Input() id: string;
  @Input() seen: boolean;
  poruka: '';
  constructor(private database: DatabaseService,
    private modalController: ModalController,
    public toastController: ToastController) { }

  ngOnInit() {
    this.markAsSeen();
  }
  markAsSeen(){
    if(this.seen === false){
      this.database.updateNotification( this.userId , {
        id: this.id,
        message: this.message,
        sender: this.sendTo,
        seen: true
      });
    }
  }
  sendRequest(){
    this.database.addNotification( this.sendTo , {
      message: this.poruka,
      sender: this.userId,
      seen: false
    })
    .then( () => {
      this.dismis();
      this.presentToast('Uspesno poslata poruka');
    })
    .catch( () => this.presentToast('Neuspesno'));
  }

  cancleRequest(){
    this.dismis();
  }
  deleteRequest(){
    this.database.deleteNotification(this.userId, this.id)
    .then( () => this.presentToast('Message was deleted'))
    .catch( () => this.presentToast('Somthing went wrong'))
    this.dismis();
  }
  dismis(){
    this.modalController.dismiss({
      dismissed: true
    });
  }
  async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      duration: 1500
    });
    toast.present();
  }
}
