import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string;
  pass: string ;

  constructor(private authentication: AuthService, private route: Router) {
    this.authentication.isAlreadyLogedIn();
  }

  ngOnInit() {

  }

  login(){
    this.authentication.login(this.email, this.pass);
  }
  registerPage(){
    this.route.navigate(['/register']);
  }
}
