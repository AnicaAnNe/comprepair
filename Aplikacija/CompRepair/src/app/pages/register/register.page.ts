import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  email: string;
  pass: string;
  ime: string;
  adresa: string;
  tipNaloga: string;
  description: string;
  servisName: string;
  phone: string;
  constructor(private authentication: AuthService) { }

  ngOnInit() {}

  register() {
    if(this.tipNaloga === undefined){
      return;
    }

    let tip;
    if (this.tipNaloga === 'servis'){
      tip = 'servis';
      this.authentication
    .registerServis(this.email, this.pass,
        {
          name: this.ime,
          adress: this.adresa,
          id : null,
          phone: this.phone,
          description: this.description,
          servisName: this.servisName,
          type: tip
        });
    }
    else{
      tip = 'klijent';
      this.authentication
    .registerKlijent(this.email, this.pass,
        {
          name: this.ime,
          id : null,
          phone: this.phone,
          type: tip
        });
    }


  }
}
