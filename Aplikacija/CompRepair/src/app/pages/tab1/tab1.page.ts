import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { NavigationStart, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { DatabaseService } from '../../services/database.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { StoreService } from 'src/app/services/store.service';
import { Servis } from 'src/model/servis';
import { AlertController, ModalController } from '@ionic/angular';
import { RequestComponent } from 'src/app/components/request/request.component';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
  public ListaUsluga: any;
  public file;
  public profileUrl: Observable<any>;
  public user$: Observable<any>;
  public servis$: Observable<any>;
  public editing = false;
  public user = {};
  constructor(
    private auth: AuthService,
    private router: Router,
    private database: DatabaseService,
    private localNotifications: LocalNotifications,
    public store: StoreService,
    public modalController: ModalController,
    public alertController: AlertController

  ) {

  }


  ngOnInit() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        if(event.url === '/tabs/tab1')
          this.store.searching = false;
        else
          this.store.searching = true;
      }
    });



      this.getUser();
  }
  confirmEdit(){
    this.database.updateUser(this.user);
    this.editing = false;
  }
  editProfile(){
    this.editing = true;
  }
  cancleEdit(){
    this.editing = false;
  }
  async obrisiKorisnika(){
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure you want to delet this user',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        }, {
          text: 'Delete',
          handler: () => {
            this.store.searchUser.subscribe(sUser => this.database.deleteUser(sUser));
          }
        }
      ]
    });

    await alert.present();
  }
  getUser(){
    if(this.store.searching){
      this.user$ = this.store.searchUser;
    }
    else{
      this.user$ = this.store.user;
    }
    this.user$.subscribe( user => {
      if(!this.store.searching){
        this.setNotificationListener(user.id);
      }
      this.getUserPhoto(user);
    });
  }
  logout() {
    this.auth.logout();
  }
  showNotifications(notifications) {
    this.localNotifications.schedule({
      id: 1,
      title: 'You have a new notification',
      text: 'click to open',
      foreground: true,
    });
  }
  setNotificationListener(id){
    this.database
    .setNotificationListener('korisnik', id)
    .subscribe((notifications) => {
       this.showNotifications(notifications);
    });
  }
  async posaljiProuku(){
    const user = await this.store.user.pipe(take(1)).toPromise();
    const searchUser = await this.store.searchUser.pipe(take(1)).toPromise();
    const modal = await this.modalController.create({
      component: RequestComponent,
      cssClass: 'small-modal',
      animated: true,
      showBackdrop: true,
      backdropDismiss: true,
      componentProps: {
        userId: user.id,
        sendTo: searchUser.id
      }
    });
    return await modal.present();
  }
  getUserPhoto(user){
    this.profileUrl = this.database.getFileUrl(
      `users/${user.id}/profile.jpg`
    );
  }
  async postaviProfilnu(){
    const user = await this.store.user.pipe(take(1)).toPromise();
    document.getElementById('fileInput').click();
    document.getElementById('fileInput').addEventListener('change', (ev: any) => {
      const file = ev.target.files[0];
      this.database.uploadFile(`users/${user.id}/profile.jpg`, file);
      setTimeout( () =>  this.getUserPhoto(user), 1500);
    });
  }
}
