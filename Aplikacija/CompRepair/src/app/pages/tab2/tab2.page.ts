import { Component } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { RequestComponent } from 'src/app/components/request/request.component';
import { DatabaseService } from 'src/app/services/database.service';
import { StoreService } from 'src/app/services/store.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  public user: Observable<any>;
  public notifikacije: Observable<any>;

  constructor(private store: StoreService,
              private db: DatabaseService,
              public modalController: ModalController,
              private router: Router,
    ) {}

  ngOnInit(){
    console.log(this.store.user);
    this.notifikacije = this.db.getNotifications(this.store.fireUser.uid);
      this.router.events.subscribe((event) => {
        if (event instanceof NavigationStart && event.url === '/tabs/tab2') {
          this.store.searching = false;
        }
      });
  }

  async openReply(notifikacija){
    const user = await this.store.user.pipe(take(1)).toPromise();
    const modal = await this.modalController.create({
      component: RequestComponent,
      cssClass: 'small-modal',
      animated: true,
      showBackdrop: true,
      backdropDismiss: true,
      componentProps: {
        seen: notifikacija.seen,
        id: notifikacija.id,
        message: notifikacija.message,
        userId: user.id,
        sendTo: notifikacija.sender
      }
    });
    return await modal.present();
  }
}
