import { Component } from '@angular/core';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { StoreService } from 'src/app/services/store.service';
import { Servis } from 'src/model/servis';
import { DatabaseService } from '../../services/database.service';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {


  searchByName = true;
  searchText: string;
  searchResults: Observable<any> = new Observable<any>();
  usluge: Observable<any>;
  constructor(private database: DatabaseService,
    private router: Router,
    private route: ActivatedRoute,
    public store: StoreService,
    public alertController: AlertController) { }

  ngOnInit(){
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart && event.url === '/tabs/tab3') {
        this.store.searching = true;
      }
    });
  }
  search() {
    this.store.searching = true;
    this.searchServiceByName();
  }
  searchServiceByName() {
    if (this.searchText)
      this.searchResults = this.database.getServiceByShopName(this.searchText);
  }
  promeniNacinPretrage() {
    if (this.searchByName) {
      console.log(this.searchByName);
    }
    else {
      if (!this.usluge)
        this.usluge = this.database.getAllUsluge();
    }
  }
  openProfile(shop){
    this.store.searchUser = this.database.getUser(shop.id);
    this.router.navigate(['profile'], { relativeTo: this.route});
  }
  async izbrisiUslugu(usluga){
    const alert = await this.alertController.create({
      header: 'Confirm!',
      message: 'Are you sure you want to delet this service',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        }, {
          text: 'Delete',
          handler: () => {
            this.database.deleteUsluga(usluga);
          }
        }
      ]
    });

    await alert.present();
  }
}


