import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { StoreService } from './store.service';
import { DatabaseService } from './database.service';
import { Servis } from 'src/model/servis';
import { Korinsik } from 'src/model/korinsik';
import { ToastController } from '@ionic/angular';

@Injectable()
export class AuthService {

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    private store: StoreService,
    private db: DatabaseService,
    public toastController: ToastController) {}

  async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      duration: 1500
    });
    toast.present();
  }

  public async getUser(fUser) {
    this.store.fireUser = fUser;
    this.store.user = this.db.getUser(fUser.uid);
    this.router.navigateByUrl('/tabs/tab1');
  }
  public deleteUser(uid){
  }
  login(email: string, password: string) {
    this.afAuth.signInWithEmailAndPassword(email, password)
    .then(user => {
      this.getUser(user.user);
    })
    .catch(err => {
      this.presentToast('Pogresan gmail ili sifra');
    });
  }
  async registerKlijent(email: string, password: string, korinsik: Korinsik) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
    .then(user => {
      this.db.addUser({...korinsik, id: user.user.uid})
      .then(() => {
      });
     this.router.navigateByUrl('/login');
    });
  }
  async registerServis(email: string, password: string, korinsik: Korinsik) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
    .then(user => {
      this.db.addUser({...korinsik, id: user.user.uid, pruzaneUsluge: []})
      .then(() => {
      });
     this.router.navigateByUrl('/login');
    });
  }

  isAlreadyLogedIn(){
    this.afAuth.authState.subscribe(user => {
          if(user != null){
            this.getUser(user);
          }
        }
      );
  }

  // googleLogin() {
  //   const provider = new firebase.auth.GoogleAuthProvider();
  //   return this.oAuthLogin(provider)
  //     .then(value => {
  //    console.log('Sucess', value),
  //    this.router.navigateByUrl('/profile');
  //  })
  //   .catch(error => {
  //     console.log('Something went wrong: ', error);
  //   });
  // }

  logout() {
    this.afAuth.signOut().then(() => {
      this.router.navigate(['/']);
    });
  }

  private oAuthLogin(provider) {
    return this.afAuth.signInWithPopup(provider);
  }


}
