import { NullTemplateVisitor } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { FirebaseApp } from '@angular/fire';
import {
  AngularFirestore,
  docChanges,
  QuerySnapshot,
} from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { forkJoin, Observable } from 'rxjs';
import { finalize, map, skip, take, tap } from 'rxjs/operators';
import { Korinsik } from 'src/model/korinsik';
import { Servis } from 'src/model/servis';
import { Usluge } from 'src/model/usluge';
import { Notifikacija } from 'src/model/notifikacija';
import { AuthService } from './auth.service';
@Injectable({

  providedIn: 'root',
})
export class DatabaseService {

  constructor(
    private db: AngularFirestore,
    private storage: AngularFireStorage,
    // private auth: AuthService
  ) {}
  getService(id: string) {

    return this.db
      .collection('korisnik')
      .doc(id)
      .get()
      .pipe(map((service: any) => service.data() ));
  }
  getUser(id: string) {
    return this.db.doc<Korinsik>(`korisnik/${id}`).valueChanges();
  }
  updateUser(user: any) {
    return this.db
      .collection('korisnik')
      .doc(user.id)
      .set(user);
  }
  deleteUser(user) {
    if(user.type === 'servis'){
      this.deleteUserFromUsluge([...user.pruzaneUsluge], user.id);
    }
    this.db
      .collection('korisnik')
      .doc(user.id)
      .delete();
  }
  addUser(user: Korinsik) {
    return this.db
      .collection('korisnik')
      .doc(user.id)
      .set(user);
  }
  getServicesByName(usluga: string) {
    return this.db
      .collection('usluge', (ref) => ref.where('name', '==', usluga))
      .get()
      .pipe(map((serviss: any) => serviss.docs.map((servis) => servis.data())));
  }

  getServiceByShopName(prodavnica: string) {
    return this.db
      .collection('korisnik', (ref) =>
        ref
          .orderBy('servisName')
          .startAt(prodavnica)
          .endAt(prodavnica + '\uf8ff')
      )
      .get()
      .pipe(map((shops: any) => shops.docs.map((shop) => shop.data())));
  }
  getServicesByReference(pointers) {
    return forkJoin(pointers.map((pointer) => this.getService(pointer)));
  }
  getAllUsluge() {
    return this.db
      .collection<Usluge[]>('usluge')
      .valueChanges( {idField : 'id'});
  }
  getUslugaByName(name: string){
    return this.db.collection<Usluge>('usluge', (ref) =>
    ref.where('name' , '==', name))
    .valueChanges( {idField : 'id'})
    .pipe(take(1));
  }
  getUslugaByShopName(usluga: string) {
    return this.db
      .collection('usluge', (ref) =>
        ref
          .orderBy('name')
          .startAt(usluga)
          .endAt(usluga + '\uf8ff')
      )
      .get()
      .pipe(map((shops: any) => shops.docs.map((shop) => {return { ...shop.data(), id: shop.id }})));
  }
  addUsluga(usluga){
    return this.db
      .collection('usluge')
      .doc()
      .set(usluga);
  }
  updateUsluga(usluga){
    return this.db
      .collection('usluge')
      .doc(usluga.id)
      .set(usluga);
  }
  deleteUsluga(usluga){
    const pointers = [...usluga.pointers];
    const name = usluga.name;
    this.db
    .collection('usluge')
    .doc(usluga.id)
    .delete();
    setTimeout( () => this.deleteUslugaFromUsers(pointers, name), 500);
  }
  deleteUslugaFromUsers(pointers, imeUsluge: string){
    pointers.forEach(userId => {
      this.db
      .collection('korisnik')
      .doc(userId)
      .valueChanges( {idField : 'id'})
      .pipe(take(1))
      .subscribe( (user: any) => {
        const index = user.pruzaneUsluge.indexOf(imeUsluge);
        if (index > -1) {
          user.pruzaneUsluge.splice(index, 1);
          this.updateUser(user);
        }
      });
    });
  }
  deleteUserFromUsluge(pruzaneUsluge, idUsera){

    pruzaneUsluge.forEach(imeUsluge => {
      this.getUslugaByName(imeUsluge).subscribe(usluga => {
        const index = usluga?.[0].pointers.indexOf(idUsera);
        if (index > -1) {
          usluga?.[0].pointers.splice(index, 1);
          this.updateUsluga(usluga?.[0]);
        }

      });
    });
  }
  uploadFile(path: string, file) {
    const task = this.storage.upload(path, file);
  }
  getFileUrl(path: string) {
    const ref = this.storage.ref(path);
    return ref.getDownloadURL();
  }
  setNotificationListener(colection: string, id: string) {
    return this.db
      .collection(colection)
      .doc(id)
      .collection('notifications')
      .snapshotChanges(['added'])
      .pipe(
        skip(1),
        map((actions) =>
          actions.map((a) => {
            if (a.payload.newIndex === 0) {
              const data = a.payload.doc.data();
              const id = a.payload.doc.id;
              return { id, ...data };
            }
          })
        )
      );
  }
  getNotifications(id: string) {
    return this.db
      .collection('korisnik')
      .doc(id)
      .collection<Notifikacija[]>('notifications')
      .valueChanges( {idField : 'id'} );
  }
  addNotification(id: string ,notification) {
    return this.db
      .collection('korisnik')
      .doc(id)
      .collection('notifications')
      .doc()
      .set(notification);
  }
  updateNotification(id: string ,notification) {
    return this.db
      .collection('korisnik')
      .doc(id)
      .collection('notifications')
      .doc(notification.id)
      .set(notification);
  }
  deleteNotification(userId: string, id: string) {
    return this.db
      .collection('korisnik')
      .doc(userId)
      .collection('notifications')
      .doc(id)
      .delete();
  }
}
