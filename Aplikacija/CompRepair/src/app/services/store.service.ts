import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  public user: any;
  public searchUser: any;
  public fireUser: any;
  public notifications = [];
  public service;
  public searching = false;
  constructor() { }
}
