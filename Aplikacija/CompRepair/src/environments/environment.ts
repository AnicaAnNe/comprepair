// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyA78QmqY08zxYn9zQD22o-qc2EOXWJX3Ek",
    authDomain: "comprepair-9b7d4.firebaseapp.com",
    projectId: "comprepair-9b7d4",
    storageBucket: "comprepair-9b7d4.appspot.com",
    messagingSenderId: "512361856032",
    appId: "1:512361856032:web:40f48a5a2fd69021105045"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
