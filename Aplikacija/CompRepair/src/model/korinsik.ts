
export interface Korinsik{
  id: string;
  name: string;
  adress?: string;
  phone: string;
  type: string;
  pruzaneUsluge?: [];
  description?: string;
  servisName?: string;
}
