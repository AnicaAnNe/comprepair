
export interface Notifikacija{
    message: string;
    seen: boolean;
    sender: string;
  }
  